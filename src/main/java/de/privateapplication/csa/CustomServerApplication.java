package de.privateapplication.csa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@ComponentScan(basePackages = {
		"de.privateapplication.csa.models",
		"de.privateapplication.csa.controller",
		"de.privateapplication.csa.config",
		"de.privateapplication.csa.services"})
@EnableMongoRepositories(value = "de.privateapplication.csa.repositorys")
@EnableWebSecurity
public class CustomServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomServerApplication.class, args);
	}

}
