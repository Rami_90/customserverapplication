package de.privateapplication.csa.config;

import de.privateapplication.csa.models.Role;
import de.privateapplication.csa.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.stream.Collectors;

@Component
class Bootstrap implements ApplicationListener<ApplicationReadyEvent> {

    private final UserService userService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public Bootstrap(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        initializeAdmin();
    }

    private void initializeAdmin() {
        if (!userService.userExist("admin")) {
            log.info("No admin user found!");
            userService.createUser("admin", generateInitialPassword(), Role.ADMIN);
        }
    }

    private String generateInitialPassword() {
        String password = new Random().ints(10, 33, 122).mapToObj(i -> String.valueOf((char)i)).collect(Collectors.joining());
        log.info("New admin password is : " + password);
        log.info("Please change the password after login");
        return password;
    }
}
