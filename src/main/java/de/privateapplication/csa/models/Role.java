package de.privateapplication.csa.models;

public enum Role {
    USER, ADMIN
}
