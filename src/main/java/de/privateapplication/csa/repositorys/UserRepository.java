package de.privateapplication.csa.repositorys;

import de.privateapplication.csa.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
}
