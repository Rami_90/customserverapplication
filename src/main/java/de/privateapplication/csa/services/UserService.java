package de.privateapplication.csa.services;

import de.privateapplication.csa.models.Role;
import de.privateapplication.csa.models.User;
import de.privateapplication.csa.repositorys.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final int MIN_PASSWORD_LENGTH = 4;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    private List<User> findAll() {
        return userRepository.findAll();
    }

    public Optional<User> findUserByName(String username) {
        return findAll().stream()
                .filter(user -> user.getUsername().equals(username))
                .findFirst();
    }

    public Boolean userExist(String username) {
        return findUserByName(username).isPresent();
    }

    public Boolean createUser(String username, String password) {
        return createUser(username, password, Role.USER);
    }

    private Boolean isPasswordValid(String password) {
        return password.length() > MIN_PASSWORD_LENGTH;
    }

    public Boolean createUser(String username, String password, Role role) {
        if (userExist(username) || !isPasswordValid(password)) return false;
        User user = new User(username, passwordEncoder.encode(password));
        user.grantAuthority(role);
        userRepository.save(user);
        return true;
    }

    public Boolean changePassword(String username, String password) {
        Optional<User> user = findUserByName(username);
        if (!user.isPresent() || !isPasswordValid(password)) return false;
        user.get().setPassword(passwordEncoder.encode(password));
        userRepository.save(user.get());
        return true;
    }
}
