package de.privateapplication.csa.controller;

import de.privateapplication.csa.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/user")
public class UserRestController {

    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/signup")
    public Boolean signUp(
            String username,
            String password
    ) { return userService.createUser(username, password); }

    @GetMapping("/signed")
    public ResponseEntity isSigned(Principal principal) {
        if (principal == null) return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
        else return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @PostMapping("/chpwd")
    public ResponseEntity changePassword(Principal principal, String password) {
        return (principal != null && userService.changePassword(principal.getName(), password)) ?
                ResponseEntity.status(HttpStatus.ACCEPTED).body(null) :
                ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }
}
