package de.privateapplication.csa.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RootRestController {

    @GetMapping("/")
    public ResponseEntity isAlive(){ return new ResponseEntity(HttpStatus.OK); }
}
